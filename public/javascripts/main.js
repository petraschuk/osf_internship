$(document).ready(function(){
	var currency = $('#picker').val();
	var price = $('#price').text();

	$('#picker').on('change', function () {
		currency = $(this).val();
		$.ajax({
			type: 'POST',
			data: {
				currency: currency,
				price: price
			}
		}).done (function (finalPrice) {
	          $('#price').html(finalPrice);
	        });;
	});

	var password = $('#userPassword').val();
	var passwordConfirm = $('#passwordConfirm').val();
	$('#userPassword').on('focusout', function() {
		password = $(this).val();
		if (password === passwordConfirm && !!password) {
			$('#registerBtn').attr('disabled', false);
		} else {
			$('#registerBtn').attr('disabled', true);
		}
	});
	$('#passwordConfirm').on('keyup', function() {
		passwordConfirm = $(this).val();
		if (password === passwordConfirm && !!password) {
			$('#registerBtn').attr('disabled', false);
		} else {
			$('#registerBtn').attr('disabled', true);
		}
	});


});
