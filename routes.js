exports.index = function(req, res) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;

	var cats;

	var userSession = req.session.userSession;
	
	mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
		var collectionCat = db.collection('categories');

		collectionCat.find({}, { id: 1, categories: 1, name: 1, _id: 0 }).toArray(function(err, ids) {
			cats = ids;
		});
		
		collectionCat.find().toArray(function(err, items) {
			res.render("index", { 
				// Underscore.js lib
				_     : _, 
				
				// Template data
				title : "Shop",
				button_text : "View products",
				items : items,
				cats: cats,
				user : userSession
			});

			db.close();
		});
	});
};

exports.category = function(req, res) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;

	var name = req.params.cat;
	var catName = name.toLowerCase();

	var cats;

	var userSession = req.session.userSession;
	
	mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
		var collectionCat = db.collection('categories');

		collectionCat.find({}, { id: 1, categories: 1, name: 1, _id: 0 }).toArray(function(err, ids) {
			cats = ids;
		});
		
		collectionCat.find({'id': catName}).toArray(function(err, items) {
			res.render("category", { 
				// Underscore.js lib
				_     : _, 
				
				// Template data
				title : "Shop",
				button_text : "View products",
				items : items,
				catName : catName,
				cats: cats,
				user : userSession
			});

			db.close();
		});
	});
};

exports.subcategory = function(req, res) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;

	var catName = req.params.cat;
	var name = req.params.subCat;
	var subCatName = name.toLowerCase();

	var cats;

	var userSession = req.session.userSession;
	
	mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
		var collectionCat = db.collection('categories');

		collectionCat.find({}, { id: 1, categories: 1, name: 1, _id: 0 }).toArray(function(err, ids) {
			cats = ids;
		});
		
		collectionCat.find({'categories.id': subCatName}).toArray(function(err, items) {
			res.render("subcategory", { 
				// Underscore.js lib
				_     : _, 
				
				// Template data
				title : "Shop",
				button_text : "View products",
				items : items,
				catName : catName,
				subCatName: subCatName,
				cats: cats,
				user : userSession
			});

			db.close();
		});
	});
};

exports.products = function(req, res) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;

	var catName = req.params.cat;
	var subCatName = req.params.subCat;
	var name = req.params.prods;
	var prodCatName = name.toLowerCase();

	var cats;

	var userSession = req.session.userSession;
	
	mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
		var collection = db.collection('products');
		var collectionCat = db.collection('categories');

		collectionCat.find({}, { id: 1, categories: 1, name: 1, _id: 0 }).toArray(function(err, ids) {
			cats = ids;
		});
		
		collection.find({primary_category_id: prodCatName}).toArray(function(err, items) {
			// products = items;

			res.render("products", {
				// Underscore.js lib
				_     : _, 
				
				// Template data
				title : "Shop",
				button_text : "More info",
				items : items,
				catName : catName,
				subCatName: subCatName,
				prodCatName : prodCatName,
				cats : cats,
				user : userSession
			});

			db.close();
		});
		
	});

};

exports.productPage = function(req, res) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;

	var catName = req.params.cat;
	var subCatName = req.params.subCat;
	var prodCatName = req.params.prods;
	var prodID = req.params.product;

	var cats;

	var userSession = req.session.userSession;
	
	mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
		var collection = db.collection('products');
		var collectionCat = db.collection('categories');

		collectionCat.find({}, { id: 1, categories: 1, name: 1, _id: 0 }).toArray(function(err, ids) {
			cats = ids;
		});
		
		collection.find({id: prodID}).toArray(function(err, items) {
			res.render("product_page", { 
				// Underscore.js lib
				_     : _, 
				
				// Template data
				title : "Shop",
				button_text : "Buy now",
				catName : catName,
				subCatName : subCatName,
				prodCatName : prodCatName,
				prodID : prodID,
				items : items,
				cats : cats, 
				user : userSession
			});

			db.close();
		});
	});
};

exports.productPagePost = function(req, res){

	var chosenCurrency = req.body.currency;
	var price = req.body.price;

	var soap = require('soap');
	var url = 'http://infovalutar.ro/curs.asmx?wsdl';
	var date = new Date();
	var _ = require("underscore");

	var currencies;
	var finalPrice;
	var curRateValue;
	var USDrateValue;

	soap.createClient(url, function(err, client) {
		client.getall({dt: date.toISOString()}, clientGetAllCallback);
	});

	function clientGetAllCallback(err, result) {
		if (err) throw err;

		var diffgram = result.getallResult.diffgram;

		// if no rate for today, back to yesterday till we get the result
		if (!diffgram) {
			date.setDate(date.getDate() - 1);

			soap.createClient(url, function (err, client) {
				client.getall({ dt: date.toISOString() }, clientGetAllCallback);
			});

			return false;
		}

		var USDrate = _.filter(diffgram.DocumentElement.Currency, function(currency) {
			return currency.IDMoneda === 'USD';
		});

		var curRate = _.filter(diffgram.DocumentElement.Currency, function(currency) {
			return currency.IDMoneda === chosenCurrency; 
		});

		if (curRate[0] === undefined) {
			curRateValue = 1;
		} else {
			curRateValue = curRate[0].Value
		}

		USDrateValue = USDrate[0].Value;
		currencies = (USDrateValue/curRateValue).toFixed(4, 10);
		finalPrice = (price * currencies).toFixed(2, 10);

		res.send(finalPrice);
	}	
};

exports.cart = function(req, res) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;

	var catName = req.params.cat;
	var subCatName = req.params.subCat;
	var prodCatName = req.params.prods;
	var prodID = req.params.product;

	var cats;

	var userSession = req.session.userSession;

	var cart = req.session.cart;
	var cartParams = {prod: [], total: 0 };
	var total = 0;

	for(var key in cart) {
		cartParams.prod.push(cart[key]);
		total += (cart[key].qty * cart[key].price);
	};

	cartParams.total = total.toFixed(2, 10);

	mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
		var collection = db.collection('products');
		var collectionCat = db.collection('categories');

		collectionCat.find({}, { id: 1, categories: 1, name: 1, _id: 0 }).toArray(function(err, ids) {
			cats = ids;
		});
		
		collection.find().toArray(function(err, items) {
			res.render("cart", { 
				// Underscore.js lib
				_     : _, 
				
				// Template data
				title : "Shop",
				button_text: 'Confirm',
				catName : catName,
				subCatName : subCatName,
				prodCatName : prodCatName,
				prodID : prodID,
				items : items,
				cats : cats,
				cart : cartParams,
				user : userSession
			});

			db.close();
		});
	});
};

exports.cartPost = function(req, res) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;

	var prodID = req.params.id;

	req.session.cart = req.session.cart || {};
	var cart = req.session.cart;

	
	mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
		var collection = db.collection('products');
		
		collection.find({id: prodID}).toArray(function(err, items) {
			if (cart[prodID]) {
				cart[prodID].qty++;
			} else {
				cart[prodID] = {
					name: items[0].name,
					price: items[0].price,
					image: items[0].image_groups,
					id: items[0].id,
					parentId: items[0].primary_category_id,
					qty: 1
				}
			}

			res.redirect('/cart');

			db.close();
		});
	});
};

exports.cartDelete = function (req, res) {
	var productId = req.params.id;
	var cart = req.session.cart;
	var product = cart[productId];

	if (product && product.qty === 1) {
		delete cart[productId];
	} else if (product && product.qty > 1) {
		product.qty -= 1;
	} else {
		// @todo: handling errors
	}

	res.redirect('/cart');
};

exports.login = function (req, res) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;

	var cats;

	var userSession = req.session.userSession;

	mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
		var collectionCat = db.collection('categories');

		collectionCat.find({}, { id: 1, categories: 1, name: 1, _id: 0 }).toArray(function(err, ids) {
			cats = ids;

			res.render('login', {
				_     : _, 
				title : "Shop",
				button_text: 'Log in',
				cats : cats,
				user : userSession,
				message : req.flash('message')
			});

			db.close();
		});
	});	
};

exports.loginPost = function (req, res) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;
	var bcrypt = require('bcrypt-nodejs');

	var cats;

	var email = req.body.userEmail;
	var password = req.body.userPassword;

	var userSession = req.session.userSession;

	mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
		var collectionUsers = db.collection('users');

		collectionUsers.find({email: email}, { _id: 0 }).toArray(function(err, user) {
			if (user[0] === undefined) {
				req.flash('message', 'Incorrect or unregistered email');
				res.redirect('/login');
			} else {
				if (bcrypt.compareSync(password, user[0].password)) {
					req.session.userSession = user[0].user;
					res.redirect('/');
				} else {
					req.flash('message', 'Incorrect password');
					res.redirect('/login');
				}
			}
			
			db.close();
		});
	});
};

exports.logout = function (req, res) {
	req.session.userSession = null;
	res.redirect('/');
};

exports.signup = function (req, res) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;

	var cats;

	var userSession = req.session.userSession;

	mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
		var collectionCat = db.collection('categories');

		collectionCat.find({}, { id: 1, categories: 1, name: 1, _id: 0 }).toArray(function(err, ids) {
			cats = ids;


			res.render('signup', {
				_     : _, 
				title : "Shop",
				button_text: 'Sign up',
				cats : cats,
				user : userSession,
				message : req.flash('message')
			});

			db.close();
		});
					
	});
};

exports.signupPost = function (req, res) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;
	var bcrypt = require('bcrypt-nodejs');

	var user = {
		name: req.body.userName,
		email: req.body.userEmail,
		password: bcrypt.hashSync(req.body.passwordConfirm)
	}

	req.session.userSession = user.name;
	var userSession = req.session.userSession;

	mdbClient.connect("mongodb://localhost:27017/shop", function(err, db) {
		var collectionUsers = db.collection('users');

		collectionUsers.insert({user: user.name, email: user.email, password: user.password}, function() {
			db.close();
		});
					
	});

	res.redirect('/cart');
};