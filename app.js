// Module dependencies.
var express = require("express");
var favicon = require("serve-favicon");
var morgan = require("morgan");
var http    = require("http");
var path    = require("path");
var routes  = require("./routes");
var bodyParser = require("body-parser");
var methodOverride = require("method-override");
var cookieParser = require("cookie-parser");
var errorHandler = require("errorhandler");
var session = require("express-session");
var flash = require('connect-flash');
var bcrypt = require('bcrypt-nodejs');
var app     = express();


// All environments
app.set("port", 8080);
app.set("views", __dirname + "/views");
app.set("view engine", "ejs");
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride(function (req, res) {
	if (req.body && typeof req.body === 'object' && '_method' in req.body) {
		var method = req.body._method;
		delete req.body._method;
		return method;
	}
}));
app.use(cookieParser("61d333a8-6325-4506-96e7-a180035cc26f"));
app.use(session({ resave: true, saveUninitialized: true, secret: 'uwotm8' }));
app.use(express.static(path.join(__dirname, "public")));
app.use(errorHandler());
app.use(morgan("dev"));
app.use(flash());

// App routes
app.get("/"                             , routes.index);
app.get("/cart"                         , routes.cart);
app.get("/login"                        , routes.login);
app.post("/cart/:id"                    , routes.cartPost);
app.delete("/cart/:id"                  , routes.cartDelete);
app.get("/login"                        , routes.login);
app.post("/login"                       , routes.loginPost);
app.get("/signup"                       , routes.signup);
app.post("/signup"                      , routes.signupPost);
app.post("/logout"                      , routes.logout);
app.get("/:cat"                         , routes.category);
app.get("/:cat/:subCat"                 , routes.subcategory);
app.get("/:cat/:subCat/:prods"          , routes.products);
app.get("/:cat/:subCat/:prods/:product" , routes.productPage);

app.post('/:cat/:subCat/:prods/:product', routes.productPagePost);

// Run server
http.createServer(app).listen(app.get("port"), function() {
	console.log("Express server listening on port " + app.get("port"));
});
